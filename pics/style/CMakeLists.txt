install(FILES
	All.png
	Anime.png
	Ancient.png
    	Baroque.png
    	Cartoon.png
	Conceptual.png
	Cyberpunk.png
	Futurism.png
	Lolita.png
	Low-Poly.png
    	Oil-Painting.png
    	Pixel.png
    	Realistic.png
    	Surrealism.png
    	Ukiyoe.png
    	Vaporwave.png
	Watercolour.png
	All@2x.png
   	Anime@2x.png
	Ancient@2x.png
    	Baroque@2x.png
	Cartoon@2x.png
	Conceptual@2x.png
	Cyberpunk@2x.png
	Futurism@2x.png
	Lolita@2x.png
	Low-Poly@2x.png
    	Oil-Painting@2x.png
    	Pixel@2x.png
	Realistic@2x.png
   	Surrealism@2x.png
    	Ukiyoe@2x.png
    	Vaporwave@2x.png
	Watercolour@2x.png
    DESTINATION ${DATA_INSTALL_DIR}/kolourpaint/pics/style)
